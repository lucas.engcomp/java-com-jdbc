package com.br.primitivos;

public class Tamanho {

    public static void main(String[] args) {

        System.out.println("Tamanhos dos tipos: \n" +
                "== Byte == \n" +
                "\n Maior: " + Byte.MAX_VALUE +
                "\t Menor: " + Byte.MIN_VALUE +

                "\n== Short == \n" +
                "\n Maior: " + Short.MAX_VALUE +
                "\t Menor: " + Short.MIN_VALUE +
                "\n =============================== \n" +

                "\n== Integer == \n" +
                "\n Maior: " + Integer.MAX_VALUE +
                "\t Menor: " + Integer.MIN_VALUE +
                "\n ================================== \n" +

                "\n== Long == \n" +
                "\n Maior: " + Long.MAX_VALUE +
                "\t Menor: " + Long.MIN_VALUE +
                "\n ================================== \n" +

                "\n== Float == \n" +
                "\n Maior: " + Float.MAX_VALUE +
                "\t Menor: " + Float.MIN_VALUE +
                "\n ================================== \n" +

                "\n== Double == \n" +
                "\n Maior: " + Double.MAX_VALUE +
                "\t Menor: " + Double.MIN_VALUE +
                "\n ================================== \n");

        System.out.println("Declaração dos tipos: \n\n");
        byte tipoByte = 127;
        short tipoShort = 32767;
        char tipoChar = 'A';
        float tipoFloat = 6.67384f;
        double tipoDouble = 3.14159265359;
        int tipoInt = 2147483647;
        long tipoLong = 9223372036854775807L;
        boolean tipoBooleano = true; // Ou  false;
        System.out.println("Valor do tipoByte = " + tipoByte);
        System.out.println("Valor do tipoShort = " + tipoShort);
        System.out.println("Valor do tipoChar = " + tipoChar);
        System.out.println("Valor do tipoFloat = " + tipoFloat);
        System.out.println("Valor do tipoDouble = " + tipoDouble);
        System.out.println("Valor do tipoInt = " + tipoInt);
        System.out.println("Valor do tipoLong = " + tipoLong);
        System.out.println("Valor do tipoBooleano = " + tipoBooleano);


    }
}
