package com.br.primitivos;

public class Conversao {

    public static void main(String[] args) {

        /**
         * Conversão implícita
         */
        double a = 1;
        System.out.println(a);

        /**
         * Conversão explícita ou (CAST)
         */
        float b = (float) 1.0;
        System.out.println(b);

        int c = 127;
        byte d = (byte) c; // quando c > 127 há perda de informações
        System.out.println(d);

        /**
         * Conversão numero para String
         */
        Integer num1 = 10000;
        System.out.println(num1.toString().length());

        int num2 = 100000;
        System.out.println(Integer.toString(num2).length());

        Long.toString(num1);

        System.out.println("" + num1);
        System.out.println("" + num2);
    }
}
