package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DAO {

    private Connection conexao;

    public int incluir(String sql, Object... atributos) {
        try {
            PreparedStatement ps = getConexao().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            if (ps.executeUpdate() > 0) {
                ResultSet resultado = ps.getGeneratedKeys();
                if (resultado.next()) {
                    return resultado.getInt(1);
                }
            }
            return -1;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void adicionarAtributos(PreparedStatement ps, Object[] atributos) throws SQLException {
        for (Object atributo: atributos) {
            int indice = 1;
            if (atributo instanceof String) {
                ps.setString(indice, (String) atributo);
            } else if (atributo instanceof Integer) {
                ps.setInt(indice, (Integer) atributo);
            }
            indice++;
        }

    }

    public void fecharConexao() {
        try {
            getConexao().close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexao = null;
        }
    }

    private Connection getConexao() {
        try {
            if (conexao != null && !conexao.isClosed()) {

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        conexao =  FabricaConexao.getConexao();
        return conexao;
     }
}
