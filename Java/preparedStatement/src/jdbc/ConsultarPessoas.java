package jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ConsultarPessoas {

    public static void main(String[] args) throws SQLException {

        Connection conexao = FabricaConexao.getConexao();
        String sql = "SELECT * FROM pessoa";

        Statement stmt = conexao.createStatement();
        stmt.execute(sql);
        ResultSet resultado = stmt.executeQuery(sql);
        List<Pessoa> pessoas = new ArrayList<>();

        while (resultado.next()) {
            Integer codigo = resultado.getInt("codigo");
            String nome = resultado.getString("nome");
            pessoas.add(new Pessoa(codigo, nome));
        }

        if (!pessoas.isEmpty()) {
            for (Pessoa p : pessoas) {
                System.out.println("Código: " + p.getCodigo() + "\n" + "Nome: " + p.getNome());
            }
        } else {
            System.out.println("Nenhuma pessoa cadastrada no momento!");
        }

        stmt.close();
        conexao.close();
    }
}
