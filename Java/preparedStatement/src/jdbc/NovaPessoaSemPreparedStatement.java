package jdbc;

import java.sql.SQLException;
import java.util.Scanner;

public class NovaPessoaSemPreparedStatement {

    public static void main(String[] args) throws SQLException {
        Scanner entrada = new Scanner(System.in);

        System.out.println("Informe o nome");
        String nome = entrada.nextLine();

        String  sql = "INSERT INTO pessoas (nome) VALUES ('" + nome + "')";

        /**
         * Ataque SQL Injection
         * INSERT INTO pessoas (nome) VALUES ('Lucas');
         * DELETE FROM pessoas where ('1' = '1');
         */

        System.out.println("Dado inserido: " + nome);
        entrada.close();
    }
}
