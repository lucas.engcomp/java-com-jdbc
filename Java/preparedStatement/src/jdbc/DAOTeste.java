package jdbc;

public class DAOTeste {

    public static void main(String[] args) {
        DAO dao = new DAO();

        String sql = "INSERT INTO pessoa (nome) VALUES (?)";
        dao.incluir(sql, "Freddie Mercury");
        dao.incluir(sql, "Roger Taylor");
        dao.incluir(sql, "Brian May");
        dao.incluir(sql, "John Deacon");
    }
}
