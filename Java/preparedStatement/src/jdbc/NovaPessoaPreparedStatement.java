package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class NovaPessoaPreparedStatement {

    public static void main(String[] args) throws SQLException {
        Scanner entrada = new Scanner(System.in);
        Connection conexao = FabricaConexao.getConexao();

        System.out.println("Informe o nome");
        String nome = entrada.nextLine();

        String  sql = "INSERT INTO pessoa (nome) VALUES (?)";
        PreparedStatement ps = conexao.prepareStatement(sql);

        ps.setString(1, nome);
        ps.execute();

        System.out.println("Dado inserido com PreparedStatement: " + nome);
        entrada.close();
    }
}
