package jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsultarPessoasComLike {

    public static void main(String[] args) throws SQLException {

        Scanner scanner = new Scanner(System.in);
        Connection conexao = FabricaConexao.getConexao();

        String sql = "SELECT * FROM pessoa WHERE nome like ?";

        System.out.println("Informe a palavra ou letra para ser pesquisa: ");
        String valorPesquisado = scanner.nextLine();

        PreparedStatement ps = conexao.prepareStatement(sql);
        ps.setString(1, "%" + valorPesquisado  + "%");
        ResultSet resultado = ps.executeQuery();

        List<Pessoa> pessoas = new ArrayList<>();

        while(resultado.next()) {
            int codigo = resultado.getInt("codigo");
            String nome = resultado.getString("nome");
            pessoas.add(new Pessoa(codigo, nome));
        }

        if (!pessoas.isEmpty()) {
            for (Pessoa p : pessoas) {
                System.out.println("Código: " + p.getCodigo() + "\n" + "Nome: " + p.getNome());
            }
        } else {
            System.out.println("Nenhuma pessoa cadastrada no momento!");
        }

        ps.close();
        conexao.close();
        scanner.close();
    }
}
