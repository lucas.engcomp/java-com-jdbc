package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class AtualizarDadosPessoa {

    public static void main(String[] args) throws SQLException {

        Scanner scanner = new Scanner(System.in);

        String selectSQL = "SELECT codigo, nome FROM pessoa where codigo = ?";
        String updateSQL = "UPDATE pessoa set nome = ? where codigo = ?";

        System.out.println("Informe o código da pessoa para atualizacao: ");
        int codigo = scanner.nextInt();

        System.out.println("Informe o novo nome da pessoa para atualizar: ");
        String nome = scanner.nextLine();

        Connection conexao = FabricaConexao.getConexao();
        PreparedStatement ps = conexao.prepareStatement(selectSQL);
        ps.setInt(1, codigo);
        ResultSet resultado = ps.executeQuery();

        if (resultado.next()) {
            Pessoa pessoa = new Pessoa(resultado.getInt(1), resultado.getString(2));
            System.out.println("O nome atual é: " + pessoa.getNome());
            scanner.nextLine();

            System.out.println("Informe o novo nome para <" + pessoa.getNome() + "> : ");
            String nomeNovo = scanner.nextLine();

            ps.close();
            /**
             * cria nova conexão com o banco
             */
            ps = conexao.prepareStatement(updateSQL);
            ps.setString(1, nomeNovo);
            ps.setInt(2, codigo);
            ps.execute();

            System.out.println("Pessoa atualizada");
        } else {
            System.out.println("Essa pessoa não foi encontrada!");
        }

        conexao.close();
        scanner.close();
    }
}
