package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class ExcluirPessoa {

    public static void main(String[] args) throws SQLException {

        Connection conexao = FabricaConexao.getConexao();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Informe o código da pessoa para realizar a exclusão: ");
        Integer codigo = scanner.nextInt();

        String sql = "DELETE FROM pessoa WHERE codigo = ?";
        PreparedStatement ps = conexao.prepareStatement(sql);

        ps.setInt(1, codigo);

        if (ps.executeUpdate() > 0) {
            System.out.println("Pessoa excluída com sucesso!");
        } else {
            System.out.println("Nenhuma pessoa excluída!");
        }

        conexao.close();
        scanner.close();

    }
}
